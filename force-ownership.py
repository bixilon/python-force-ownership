import grp
import json
import os
import pwd
import sys
import threading

from watchdog.events import FileSystemEventHandler, EVENT_TYPE_CREATED, EVENT_TYPE_MOVED
from watchdog.observers import Observer

CONFIG = json.load(open(sys.argv[1]))


class Watcher(FileSystemEventHandler):

    def __init__(self, directory, options):
        self.directory = directory
        self.options = options
        self.observer = Observer()

    def watch(self):
        self.observer.schedule(self, self.directory, recursive=self.options["recursive"])
        self.observer.start()
        self.observer.join()

    def on_any_event(self, event):
        if event.event_type != EVENT_TYPE_CREATED and event.event_type != EVENT_TYPE_MOVED:
            return None
        path = event.src_path
        if event.event_type == EVENT_TYPE_MOVED:
            path = event.dest_path

        if path.startswith("~") or path.endswith(".swp") or path.endswith(".lock") or path.endswith(".tmp"):
            return None

        try:
            if "uid" in self.options:
                os.chown(path, self.options["uid"], self.options["gid"])
            if "mode" in self.options:
                os.chmod(path, self.options["mode"])
        except Exception as e:
            print("Error: %s" % path)
            print(e)
            return None

        print("Passed file (%s) %s" % (event.event_type, path))


if __name__ == '__main__':
    for directory in CONFIG:
        print("Watching %s" % directory)
        options = {
            "recursive": True
        }
        if "user" in CONFIG[directory]:
            user = CONFIG[directory]["user"]
            if type(user) == int:
                options["uid"] = user
            else:
                options["uid"] = pwd.getpwnam(user).pw_uid
            group = CONFIG[directory]["group"]
            if type(group) == int:
                options["gid"] = group
            else:
                options["gid"] = grp.getgrnam(group).gr_gid

        if "recursive" in CONFIG[directory]:
            options["recursive"] = CONFIG[directory]["recursive"]

        if "mode" in CONFIG[directory]:
            string = str(CONFIG[directory]["mode"])
            options["mode"] = int(string, base=8)

        watcher = Watcher(directory, options)
        threading.Thread(target=watcher.watch).start()
