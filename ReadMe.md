# Python force ownership

This tool allows you to force the ownership on a specific directory. **Must** run as root.

It should probably run as systemd service:

`/etc/systemd/system/force-ownership.service`:

```
[Unit]
Description=force-ownership

[Service]
User=root
Group=root
WorkingDirectory=<Path to the base folder of the script>
LimitNOFILE=4096
ExecStart=python3 force-ownership.py <Path to config file>
Type=simple
Restart=always
RestartSec=3
StartLimitInterval=600

[Install]
WantedBy=multi-user.target
```

Followed by a
`sudo systemctl enable --now force-ownership.service`

You should maybe increase the inotify limit:

```
sudo sysctl fs.inotify.max_user_watches=524288
```
